import { createGlobalStyle } from 'styled-components';
import US700 from 'assets/fonts/Untitled_Sans_Bold.woff2';
import US500 from 'assets/fonts/Untitled_Sans_Medium.woff2';
import US400 from 'assets/fonts/Untitled_Sans_Regular.woff2';
import US300 from 'assets/fonts/Untitled_Sans_Light.woff2';

const GlobalStyle = createGlobalStyle`

  @font-face {
    font-family: 'Untitled Sans';
    src: url(${US300}) format('truetype');
    font-weight: 300;
    font-style: normal;
  }

  @font-face {
    font-family: 'Untitled Sans';
    src: url(${US400}) format('truetype');
    font-weight: 400;
    font-style: normal;
  }
  @font-face {
    font-family: 'Untitled Sans';
    src: url(${US500}) format('truetype');
    font-weight: 500;
    font-style: normal;
  }
  @font-face {
    font-family: 'Untitled Sans';
    src: url(${US700}) format('truetype');
    font-weight: 700;
    font-style: normal;
  }
  html,
  body {
    height: 100%;
    width: 100%;

  }

  body {
    font-family: 'Untitled Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    min-height: 100%;
    ${'' /* Due to limited time, the app is not made to be responsive :/ */}
    min-width: 1200px;
    width: 100%;
    height: 100%;
  }

  button {
    background: #ab61e5;
    border-radius: 4px;
    color: #fff;
    padding: 15px 25px;
    font-size: 16px;
    line-height: 19px;
    border: 1px solid transparent;
    cursor: pointer;
    transition: all 0.3s;
    &:hover {
      border: 1px solid #ab61e5;
      color: #ab61e5;
      background: #fff;
    }
    &.secondary {
      background: #fff;
      border: 1px solid #D9DCDE;
      color: #000;
      &:hover {
        background: #F2F3F4;
        color: #000;
      }
    }
  }

`;

export default GlobalStyle;
