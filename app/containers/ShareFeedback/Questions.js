import React from 'react';
import { Link } from 'react-router-dom';
import StepWizard from 'react-step-wizard';
import styled from 'styled-components';
import Step from 'components/QuestionStep';

const StyledQuestions = styled.div`
  max-width: 780px;
  margin: auto;
  .animation-wizard {
    &.in {
      opacity: 1;
      transition: all 0.37s;
    }
    &.out {
      opacity: 0;
    }
  }
  .back-button {
    font-size: 12px;
    line-height: 14px;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    color: #59636e;
    text-decoration: none;
    font-weight: 500;
    display: block;
    margin: 1rem 0;
    i {
      border: solid #59636e;
      border-width: 0 3px 3px 0;
      display: inline-block;
      padding: 3px;
      transform: rotate(135deg);
      margin-right: 10px;
    }
  }
`;

const custom = {
  enterRight: 'animation-wizard in',
  enterLeft: 'animation-wizard in',
  exitRight: 'animation-wizard out',
  exitLeft: 'animation-wizard out',
};

export default function Questions({ feedbacks, id, onComplete }) {
  const feedback = feedbacks.find(f => f.id === id);
  const qlist = feedback && feedback.questions;

  return (
    <StyledQuestions>
      <Link to="/share-feedback" className="back-button">
        <i />
        Back
      </Link>
      {qlist ? (
        <StepWizard transitions={custom}>
          {qlist.map((q, qid) => (
            <Step
              question={q}
              key={qid}
              feedback={feedback}
              qid={qid}
              onComplete={onComplete}
            />
          ))}
        </StepWizard>
      ) : (
        ''
      )}
    </StyledQuestions>
  );
}
