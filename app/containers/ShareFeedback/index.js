/**
 *
 * ShareFeedback
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import GeneralLayout from 'components/GeneralLayout';
import EmptyFeedback from 'components/EmptyFeedback';
import ShareList from 'components/ShareList';

import selectFeedbacks from 'containers/Feedbacks/selectors';
import { selectUser } from 'containers/Login/selectors';
import reducer from 'containers/Feedbacks/reducer';
import saga from 'containers/Feedbacks/saga';
import { updateFeedback } from 'containers/Feedbacks/actions';
import Questions from './Questions';

const key = 'feedbacks';

export function ShareFeedback({ feedbacks, user, match, history, dispatch }) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const showThanks = match.params.feedbackID === 'continue';
  const feedbackID = !showThanks && match.params.feedbackID;
  const filtered = feedbacks.filter(f => f.from === user.username);

  const onComplete = feedback => {
    feedback.completed = true;
    dispatch(updateFeedback(feedback));
    history.push('/share-feedback/continue');
  };

  return (
    <GeneralLayout>
      <Helmet>
        <title>Share Feedback - Honesto App</title>
        <meta name="description" content="Description of ShareFeedback" />
      </Helmet>
      {feedbackID ? (
        <Questions
          feedbacks={filtered}
          id={feedbackID}
          onComplete={onComplete}
        />
      ) : (
        <div style={{ maxWidth: '780px', margin: '0 auto'}}>
          {showThanks ? (
            <div>
              <h1>Thank you for sharing your feedback!</h1>
              <div>Continue to give feedback to other team members.</div>
            </div>
          ) : (
            ''
          )}
          {filtered.length === 0 ? (
            <EmptyFeedback />
          ) : !showThanks ? (
            <h1>Share Feedback</h1>
          ) : null}
          <ShareList feedbacks={filtered}></ShareList>
        </div>
      )}
    </GeneralLayout>
  );
}

ShareFeedback.propTypes = {
  feedbacks: PropTypes.array,
  user: PropTypes.object,
  match: PropTypes.object,
  history: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  feedbacks: selectFeedbacks(),
  user: selectUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(ShareFeedback);
