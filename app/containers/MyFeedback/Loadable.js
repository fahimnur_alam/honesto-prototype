/**
 * Asynchronously loads the component for MyFeedback
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
