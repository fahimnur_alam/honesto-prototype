/**
 *
 * MyFeedback
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import GeneralLayout from 'components/GeneralLayout';

import selectFeedbacks from 'containers/Feedbacks/selectors';
import { selectUser } from 'containers/Login/selectors';
import reducer from 'containers/Feedbacks/reducer';
import saga from 'containers/Feedbacks/saga';

import EmptyFeedback from 'components/EmptyFeedback';
import DisplayFeedbacks from 'components/DisplayFeedbacks';

const key = 'feedbacks';

export function MyFeedback({ feedbacks, user, match }) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const filtered = feedbacks.filter(
    f => f.from === user.username && f.completed,
  );
  const feedbackID = match.params.feedbackID;
  return (
    <GeneralLayout>
      <Helmet>
        <title>My Feedback - Honesto App</title>
        <meta name="description" content="Description of Honesto App" />
      </Helmet>
      {filtered.length === 0 ? (
        <EmptyFeedback />
      ) : (
        <DisplayFeedbacks feedbacks={filtered} type={'my'} id={feedbackID}/>
      )}
    </GeneralLayout>
  );
}

MyFeedback.propTypes = {
  feedbacks: PropTypes.array,
  user: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  feedbacks: selectFeedbacks(),
  user: selectUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(MyFeedback);
