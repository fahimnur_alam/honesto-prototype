/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 */

import React from 'react';
import GeneralLayout from 'components/GeneralLayout';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const StyledBox = styled.div`
  padding: 5rem;
  background: #ffffff;
  box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
`;

export default function NotFound() {
  return (
    <GeneralLayout>
      <StyledBox>
        <span>404</span>
        <h1>Sorry! The page you are looking for cannot be found. 😢</h1>
        <Link to="/share-feedback">
          <button>Back to Share Feedback</button>
        </Link>
      </StyledBox>
    </GeneralLayout>
  );
}
