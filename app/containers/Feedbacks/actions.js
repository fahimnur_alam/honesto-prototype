/*
 *
 * Feedback actions
 *
 */

import {
  GET_FEEDBACKS,
  GET_FEEDBACKS_SUCCESS,
  GET_FEEDBACKS_ERROR,
  UPDATE_FEEDBACK,
  UPDATE_FEEDBACK_SUCCESS,
  UPDATE_FEEDBACK_ERROR,
  RESET_FEEDBACKS,
  RESET_FEEDBACKS_SUCCESS,
} from './constants';

export function getFeedbacks() {
  return {
    type: GET_FEEDBACKS,
  };
}

export function getFeedbacksSuccess(feedbacks) {
  return {
    type: GET_FEEDBACKS_SUCCESS,
    feedbacks,
  };
}

export function getFeedbacksError() {
  return {
    type: GET_FEEDBACKS_ERROR,
  };
}

export function updateFeedback(feedback) {
  return {
    type: UPDATE_FEEDBACK,
    feedback,
  };
}

export function updateFeedbackSuccess(feedbacks) {
  return {
    type: UPDATE_FEEDBACK_SUCCESS,
    feedbacks,
  };
}

export function updateFeedbackError() {
  return {
    type: UPDATE_FEEDBACK_ERROR,
  };
}

export function resetFeedbacks() {
  return {
    type: RESET_FEEDBACKS,
  };
}
