export const seed = [
  {
    id: 'A98ZB973KW',
    from: 'nathan',
    completed: true,
    to: 'jane',
    name: 'Nathanial Moon',
    avatar: 'nathanial-moon',
    created: new Date('2019-01-10'),
    questions: [
      {
        title:
          'How well did Jane do X thing last month Lorem ipsum dolor sit amet Lorem ipsum dolor?',
        rating: 3,
        type: 'options',
        options: [
          'Please Improve You may have done one or the following: Maybe you were mostly quiet in meetings and when you had something on your mind, you brought it to the team afterward. Or, you had feedback that would be valuable to go, but you found it too difficult. Or, you had an opportunity to grow by doing something uncomfortable but you didn’t',
          'You Were Good You sometimes participate in meetings but you feel that they don’t always bring up important things when they should.',
          'You Were Great I and others can count on your courage to help the team do what is right.',
        ],
        skipped: false,
        value: 3,
      },
      {
        title:
          'How would you rate the quality of X Lorem ipsum dolor sit amet?',
        rating: 1,
        type: 'rating',
        value: 9,
        skipped: true,
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
      },
      {
        title: 'What do you think of Fahim?',
        rating: 2,
        type: 'text',
        value:
          "I feel as if he had more time, he could have definitely done a better job on this application, but so far it's pretty cool",
      },
    ],
  },
  {
    id: 'A98DB973KW',
    from: 'chris',
    completed: true,
    to: 'jane',
    name: 'Chris Johnson',
    avatar: 'chris-johnson',
    created: new Date('2019-01-15'),
    questions: [
      {
        title:
          'How well did Jane do X thing last month Lorem ipsum dolor sit amet Lorem ipsum dolor?',
        rating: 3,
        type: 'options',
        options: [
          'Please Improve You may have done one or the following: Maybe you were mostly quiet in meetings and when you had something on your mind, you brought it to the team afterward. Or, you had feedback that would be valuable to go, but you found it too difficult. Or, you had an opportunity to grow by doing something uncomfortable but you didn’t',
          'You Were Good You sometimes participate in meetings but you feel that they don’t always bring up important things when they should.',
          'You Were Great I and others can count on your courage to help the team do what is right.',
        ],
        skipped: false,
        value: 2,
      },
      {
        title:
          'How would you rate the quality of X Lorem ipsum dolor sit amet?',
        rating: 1,
        type: 'rating',
        value: 2,
        skipped: true,
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
      },
      {
        title: 'How well did I display courage?',
        rating: 2,
        type: 'text',
        value:
          'You Were Great I and others can count on your courage to help the team do what is right.',
      },
    ],
  },
  {
    id: 'A9XDB973KW',
    from: 'jane',
    completed: true,
    to: 'chris',
    name: 'Chris Johnson',
    avatar: 'chris-johnson',
    created: new Date('2019-01-15'),
    questions: [
      {
        title:
          'How well did Chris do X thing last month Lorem ipsum dolor sit amet Lorem ipsum dolor?',
        rating: 3,
        type: 'options',
        options: [
          'Please Improve You may have done one or the following: Maybe you were mostly quiet in meetings and when you had something on your mind, you brought it to the team afterward. Or, you had feedback that would be valuable to go, but you found it too difficult. Or, you had an opportunity to grow by doing something uncomfortable but you didn’t',
          'You Were Good You sometimes participate in meetings but you feel that they don’t always bring up important things when they should.',
          'You Were Great I and others can count on your courage to help the team do what is right.',
        ],
        skipped: false,
        value: 2,
      },
      {
        title:
          'How would you rate the quality of X Lorem ipsum dolor sit amet?',
        rating: 1,
        type: 'rating',
        value: 2,
        skipped: true,
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
      },
      {
        title: 'How well did I display courage?',
        rating: 2,
        type: 'text',
        value:
          'You Were Great I and others can count on your courage to help the team do what is right.',
      },
    ],
  },
  {
    id: 'B981B973KW',
    from: 'jane',
    completed: false,
    to: 'denis',
    avatar: 'denis-denison',
    name: 'Denis Denison',
    created: new Date('2019-01-15'),
    questions: [
      {
        title:
          'How well did Denis do X thing last month Lorem ipsum dolor sit amet Lorem ipsum dolor?',
        rating: 3,
        type: 'options',
        options: [
          'Please Improve You may have done one or the following: Maybe you were mostly quiet in meetings and when you had something on your mind, you brought it to the team afterward. Or, you had feedback that would be valuable to go, but you found it too difficult. Or, you had an opportunity to grow by doing something uncomfortable but you didn’t',
          'You Were Good You sometimes participate in meetings but you feel that they don’t always bring up important things when they should.',
          'You Were Great I and others can count on your courage to help the team do what is right.',
        ],
        skipped: false,
        value: null,
      },
      {
        title:
          'How would you rate the quality of X Lorem ipsum dolor sit amet?',
        rating: 1,
        type: 'rating',
        value: null,
        skipped: false,
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
      },
      {
        title: 'How well did I display courage?',
        rating: 2,
        type: 'text',
        value: '',
        skipped: false,
      },
    ],
  },
  {
    id: 'Z981B973KW',
    from: 'jane',
    completed: false,
    to: 'nathanial',
    avatar: 'nathanial-moon',
    name: 'Nathanial Moon',
    created: new Date('2019-01-15'),
    questions: [
      {
        title:
          'How well did this person do X thing last month Lorem ipsum dolor sit amet Lorem ipsum dolor?',
        rating: 3,
        type: 'options',
        options: [
          'Please Improve You may have done one or the following: Maybe you were mostly quiet in meetings and when you had something on your mind, you brought it to the team afterward. Or, you had feedback that would be valuable to go, but you found it too difficult. Or, you had an opportunity to grow by doing something uncomfortable but you didn’t',
          'You Were Good You sometimes participate in meetings but you feel that they don’t always bring up important things when they should.',
          'You Were Great I and others can count on your courage to help the team do what is right.',
        ],
        skipped: false,
        value: null,
      },
      {
        title:
          'How would you rate the quality of X Lorem ipsum dolor sit amet?',
        rating: 1,
        type: 'rating',
        value: null,
        skipped: false,
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
      },
      {
        title: 'How well did I display courage?',
        rating: 2,
        type: 'text',
        value: '',
        skipped: false,
      },
    ],
  },
];
