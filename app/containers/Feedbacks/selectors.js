import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the shareFeedback state domain
 */

const selectFeedbacksDomain = state => state.feedbacks || initialState;

/**
 * Other specific selectors
 */
const selectFeedbacks = () =>
  createSelector(
    selectFeedbacksDomain,
    domainState => domainState.feedbacks,
  );

export default selectFeedbacks;
export { selectFeedbacksDomain };
