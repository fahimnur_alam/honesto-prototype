/*
 *
 * Feedback reducer
 *
 */
import produce from 'immer';
import store from 'store';

import {
  GET_FEEDBACKS,
  GET_FEEDBACKS_SUCCESS,
  GET_FEEDBACKS_ERROR,
  UPDATE_FEEDBACK,
  UPDATE_FEEDBACK_SUCCESS,
  UPDATE_FEEDBACK_ERROR,
  RESET_FEEDBACKS,
} from './constants';

export const initialState = {
  loading: false,
  updating: false,
  error: false,
  feedbacks: store.get('feedbacks') || [],
};

/* eslint-disable default-case, no-param-reassign */
const feedbackReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_FEEDBACKS:
        draft.loading = true;
        break;
      case UPDATE_FEEDBACK:
        draft.updating = true;
        break;
      case UPDATE_FEEDBACK_SUCCESS:
        draft.feedbacks = action.feedbacks;
        break;
      case GET_FEEDBACKS_SUCCESS:
        draft.feedbacks = action.feedbacks;
        break;
      case UPDATE_FEEDBACK_ERROR:
      case GET_FEEDBACKS_ERROR:
        draft.loading = true;
        draft.error = 'Unable to complete the action';
        break;
      case RESET_FEEDBACKS:
        draft.feedbacks.length = 0;
        draft.loading = false;
        break;
    }
  });

export default feedbackReducer;
