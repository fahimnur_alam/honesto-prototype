import { select, delay, takeLatest, put } from 'redux-saga/effects';
import store from 'store';

import selectFeedbacks from './selectors';

import { updateFeedbackSuccess } from './actions';

import { UPDATE_FEEDBACK } from './constants';

// Individual exports for testing

export function* updateFeedback({ feedback }) {
  yield delay(500);
  const feedbacks = yield select(selectFeedbacks());
  let foundIndex = null;
  feedbacks.forEach((f, i) => {
    const found = f.id === feedback.id;
    if (found) foundIndex = i;
    return found;
  });
  feedbacks[foundIndex] = feedback;
  yield put(updateFeedbackSuccess(feedbacks));
  store.set('feedbacks', feedbacks);
}

export default function* feedbackSaga() {
  yield takeLatest(UPDATE_FEEDBACK, updateFeedback);
}
