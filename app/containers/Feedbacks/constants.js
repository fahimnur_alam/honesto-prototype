/*
 *
 * Feedback constants
 *
 */

export const GET_FEEDBACKS = 'app/Feedback/GET_FEEDBACKS';
export const GET_FEEDBACKS_SUCCESS = 'app/Feedback/GET_FEEDBACKS_SUCCESS';
export const GET_FEEDBACKS_ERROR = 'app/Feedback/GET_FEEDBACKS_ERROR';

export const UPDATE_FEEDBACK = 'app/Feedback/UPDATE_FEEDBACK';
export const UPDATE_FEEDBACK_SUCCESS = 'app/Feedback/UPDATE_FEEDBACK_SUCCESS';
export const UPDATE_FEEDBACK_ERROR = 'app/Feedback/UPDATE_FEEDBACK_ERROR';

export const RESET_FEEDBACKS = 'app/Feedback/RESET_FEEDBACKS';
export const RESET_FEEDBACKS_SUCCESS = 'app/Feedback/RESET_FEEDBACKS_SUCCESS';
