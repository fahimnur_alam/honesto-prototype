/*
 *
 * Login reducer
 *
 */
import produce from 'immer';
import store from 'store';
import {
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  LOGOUT_USER,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_ERROR,
} from './constants';

export const initialState = {
  loggedIn: store.get('token'),
  loading: false,
  error: false,
  user: store.get('user') || {},
};

/* eslint-disable default-case, no-param-reassign */
const loginReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case LOGOUT_USER:
      case LOGIN_USER:
        draft.loading = true;
        draft.error = false;
        draft.user = {};
        break;
      case LOGIN_USER_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.loggedIn = true;
        draft.user = action.user;
        break;
      case LOGOUT_USER_ERROR:
      case LOGIN_USER_ERROR:
        draft.loading = false;
        draft.error = 'Unable to login the user';
        break;
      case LOGOUT_USER_SUCCESS:
        draft.loggedIn = false;
        draft.user = {};
        draft.loading = false;
        draft.error = false;
        break;
    }
  });

export default loginReducer;
