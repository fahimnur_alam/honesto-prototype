/*
 *
 * Login constants
 *
 */

export const LOGIN_USER = 'app/Login/LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'app/Login/LOGIN_USER_SUCCESS';
export const LOGIN_USER_ERROR = 'app/Login/LOGIN_USER_ERROR';

export const LOGOUT_USER = 'app/Login/LOGOUT_USER';
export const LOGOUT_USER_SUCCESS = 'app/Login/LOGOUT_USER_SUCCESS';
export const LOGOUT_USER_ERROR = 'app/Login/LOGOUT_USER_ERROR';
