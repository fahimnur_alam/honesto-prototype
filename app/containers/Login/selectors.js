import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the login state domain
 */

const selectLoginDomain = state => state.login || initialState;

/**
 * Other specific selectors
 */

const selectUser = () =>
  createSelector(
    selectLoginDomain,
    domainState => domainState.user,
  );

const selectUserStatus = () =>
  createSelector(
    selectLoginDomain,
    domainState => domainState.loggedIn,
  );
/**
 * Default selector used by Login
 */

const makeSelectLogin = () =>
  createSelector(
    selectLoginDomain,
    substate => substate,
  );

export default makeSelectLogin;
export { selectLoginDomain, selectUser, selectUserStatus };
