/**
 *
 * Login
 *
 */

import React, { memo } from 'react';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from 'styled-components';

import Footer from 'components/Footer';
import appIcon from 'assets/images/app-icon.svg';
import background from 'assets/images/background.jpg';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectLogin from './selectors';
import { loginUser, logoutUser } from './actions';
import reducer from './reducer';
import saga from './saga';

const key = 'login';

const StyledLoginDiv = styled.div`
  min-height: 100vh;
  background-image: url(${background});
  background-size: cover;
  overflow: hidden;
  ::-webkit-scrollbar {
    display: none;
  }
  .auth {
    background-color: #ffffff;
    box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
    display: flex;
    align-items: center;
    flex-direction: column;
    height: 382px;
    justify-content: center;
    left: 50%;
    margin: -230px 0 0 -190px;
    position: absolute;
    top: 50%;
    width: 380px;
  }
  h1 {
    font-size: 24px;
    padding-bottom: 40px;
  }
  img.app {
    height: 70px;
    width: 70px;
  }
  .login-footer {
    position: absolute;
    bottom: 0;
    width: 100%;
  }
`;

export function Login({ state, login }) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const { loggedIn, loading } = state;

  return loggedIn ? (
    <Redirect to="/share-feedback" />
  ) : (
    <StyledLoginDiv>
      <Helmet>
        <title>Login - Honesto App</title>
        <meta name="description" content="Description of Login" />
      </Helmet>
      <div className="auth">
        <img className="app" src={appIcon} />
        <h1>Honesto</h1>
        <button onClick={login} type="button" disabled={loading}>
          Login with Google
        </button>
      </div>
      <div className="login-footer">
        <Footer />
      </div>
    </StyledLoginDiv>
  );
}

Login.propTypes = {
  state: PropTypes.object,
  login: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  state: makeSelectLogin(),
});

function mapDispatchToProps(dispatch) {
  return {
    login: () => {
      dispatch(loginUser());
    },
    logout: () => {
      dispatch(logoutUser());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Login);
