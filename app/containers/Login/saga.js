import { put, delay, takeLatest } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import store from 'store';

import {
  getFeedbacksSuccess,
  resetFeedbacks,
} from 'containers/Feedbacks/actions';
import { seed as feedbackSeed } from 'containers/Feedbacks/seed';
import { seed as userSeed } from './seed';

import {
  loginError,
  loginSuccess,
  logoutSuccess,
  logoutError,
} from './actions';

import { LOGIN_USER, LOGOUT_USER } from './constants';

// Individual exports for testing

export function* loginUser() {
  try {
    store.set('token', 'secretTOKEN 😎');
    store.set('user', userSeed);
    store.set('feedbacks', feedbackSeed);
    yield delay(500);
    yield put(loginSuccess(userSeed));
    yield put(getFeedbacksSuccess(feedbackSeed));
  } catch (err) {
    // can pass in err from end point
    yield put(loginError());
  }
}

export function* logoutUser() {
  try {
    yield put(push('/login'));
    store.remove('token');
    store.remove('user');
    store.remove('feedbacks');
    yield put(logoutSuccess());
    yield put(resetFeedbacks());
  } catch (err) {
    // can pass in err from end point
    yield put(logoutError());
  }
}

export default function* loginSaga() {
  yield takeLatest(LOGIN_USER, loginUser);
  yield takeLatest(LOGOUT_USER, logoutUser);
}
