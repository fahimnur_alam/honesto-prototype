/*
 *
 * Login actions
 *
 */

import {
  LOGIN_USER,
  LOGOUT_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  LOGOUT_USER_ERROR,
  LOGOUT_USER_SUCCESS,
} from './constants';

export function loginUser() {
  return {
    type: LOGIN_USER,
  };
}

export function loginSuccess(user) {
  return {
    type: LOGIN_USER_SUCCESS,
    user,
  };
}

export function loginError() {
  return {
    type: LOGIN_USER_ERROR,
  };
}

export function logoutError() {
  return {
    type: LOGOUT_USER_ERROR,
  };
}

export function logoutUser() {
  return {
    type: LOGOUT_USER,
  };
}

export function logoutSuccess() {
  return {
    type: LOGOUT_USER_SUCCESS,
  };
}
