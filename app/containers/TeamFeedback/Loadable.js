/**
 * Asynchronously loads the component for TeamFeedback
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
