/**
 *
 * TeamFeedback
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import GeneralLayout from 'components/GeneralLayout';
import DisplayFeedbacks from 'components/DisplayFeedbacks';

import selectFeedbacks from 'containers/Feedbacks/selectors';
import { selectUser } from 'containers/Login/selectors';
import reducer from 'containers/Feedbacks/reducer';
import saga from 'containers/Feedbacks/saga';
import EmptyFeedback from 'components/EmptyFeedback'

const key = 'feedbacks';

export function TeamFeedback({ feedbacks, user, match}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const filtered = feedbacks.filter(f => {
    return !(f.from === user.username);
  });
  const feedbackID = match.params.feedbackID;
  return (
      <GeneralLayout>
      <Helmet>
        <title>Team Feedback - Honesto App</title>
        <meta name="description" content="Description of TeamFeedback" />
      </Helmet>
        {filtered.length === 0 ? <EmptyFeedback />: 
          <DisplayFeedbacks feedbacks={filtered} type={'team'} id={feedbackID}/>        
        }
      </GeneralLayout>
  );
}

TeamFeedback.propTypes = {
  feedbacks: PropTypes.array,
  user: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  feedbacks: selectFeedbacks(),
  user: selectUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(TeamFeedback);
