/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Login from 'containers/Login/Loadable';
import ShareFeedback from 'containers/ShareFeedback/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import PrivateRoute, { isLoggedIn } from 'utils/privateRoute';
import MyFeedback from 'containers/MyFeedback/Loadable';
import TeamFeedback from 'containers/TeamFeedback/Loadable';
import GlobalStyle from '../../global-styles';
import 'normalize.css';

export default function App() {
  return (
    <>
      <Switch>
        <Route exact path="/" component={() => <Redirect to="/login" />} />
        <Route
          exact
          path="/login"
          render={props =>
            isLoggedIn(props) ? <Redirect to="/share-feedback" /> : <Login />
          }
        />
        <PrivateRoute
          path="/share-feedback/:feedbackID?"
          component={ShareFeedback}
        />

        <PrivateRoute
          exact
          path="/my-feedback/:feedbackID?"
          component={MyFeedback}
        />
        <PrivateRoute
          exact
          path="/team-feedback/:feedbackID?"
          component={TeamFeedback}
        />

        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </>
  );
}
