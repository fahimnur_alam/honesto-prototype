/**
 *
 * Header
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { NavLink } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import { logoutUser } from 'containers/Login/actions';
import reducer from 'containers/Login/reducer';
import saga from 'containers/Login/saga';
import { selectUser } from 'containers/Login/selectors';

import styled from 'styled-components';
import avatar from 'assets/avatars/jane-smith.svg';

const key = 'login';

const StyledHeader = styled.div`
  background-color: #f2f3f4;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.25);
  display: flex;
  height: 75px;
  align-items: center;
  justify-content: center;
`;

const StyledNav = styled.nav`
  align-items: center;
  display: flex;
  font-size: 16px;
  height: 100%;
  justify-content: space-between;
  width: 100%;
  max-width: 1200px;
  padding: 0 1rem;
  .navbar-logo {
    justify-self: start;
    font-weight: 700;
    font-size: 24px;
    margin-right: 40px;
  }
  .navbar-links {
    align-items: center;
    display: flex;
    font-weight: 500;
    height: 100%;
    font-size: 16px;
    .navbar-link {
      display: flex;
      align-items: center;
      justify-content: center;
      margin-right: 40px;
      border-bottom: solid 3px transparent;
      text-decoration: none;
      color: #031323;
      height: 100%;
      &:visited {
        color: #000;
      }
      &.navbar-link--active,
      &:hover {
        border-bottom: solid 3px #ab61e5;
      }
    }
  }
  .navbar-cycle {
    text-align: right;
    .navbar-cycle--text {
      color: #59636e;
      font-size: 11px;
      padding-bottom: 5px;
    }
    .navbar-cycle--date {
      color: #21b7a2;
      font-weight: 500;
    }

    margin: 0 15px 0 auto;
  }
  .navbar--user {
    align-items: center;
    border-left: 1px solid #d9dcde;
    display: flex;
    height: 100%;
    justify-content: center;
    .navbar--user-photo {
      align-items: center;
      background-color: #ffffff;
      border-radius: 99px;
      display: flex;
      height: 58px;
      justify-content: center;
      margin: 0 15px;
      overflow: hidden;
      width: 58px;
      img {
        height: 58px;
      }
    }
    .navbar--username {
      .navbar--logout {
        cursor: pointer;
        font-size: 12px;
        letter-spacing: 0.15em;
        text-transform: uppercase;
        color: #59636e;
      }
    }
  }
`;

function Header({ logout, user }) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const handleLogout = () => {
    logout();
  };
  const links = [
    {
      title: 'Share Feedback',
      path: '/share-feedback',
    },
    {
      title: 'My Feedback',
      path: '/my-feedback',
    },
    {
      title: 'Team Feedback',
      path: '/team-feedback',
    },
    {
      title: 'Teams',
      path: '/teams',
    },
  ];
  return (
    <StyledHeader>
      <StyledNav>
        <div className="navbar-logo">Honesto</div>

        <div className="navbar-links">
          {links.map(t => (
            <NavLink
              className="navbar-link"
              activeClassName="navbar-link--active"
              to={t.path}
              key={t.path}
            >
              {t.title}
            </NavLink>
          ))}
        </div>

        <div className="navbar-cycle">
          <div className="navbar-cycle--text">Next Feedback Cycle:</div>
          <div>
            Sept 2018 – <span className="navbar-cycle--date">4 days</span>
          </div>
        </div>
        <div className="navbar--user">
          <div className="navbar--user-photo">
            <img src={avatar} alt="Jane Smith" />
          </div>
          <div className="navbar--username">
            <div>{user.name}</div>
            <a className="navbar--logout" onClick={handleLogout}>
              Logout
            </a>
          </div>
        </div>
      </StyledNav>
    </StyledHeader>
  );
}

Header.propTypes = {
  logout: PropTypes.func,
  user: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  user: selectUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    logout: () => {
      dispatch(logoutUser());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Header);
