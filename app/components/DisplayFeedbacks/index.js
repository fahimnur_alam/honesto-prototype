/**
 *
 * DisplayFeedbacks
 *
 */

import React, { memo, useState } from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components';
import Rating from 'react-rating';

const loadImage = fileName => (
  <img src={require(`assets/avatars/${fileName}.svg`)} alt={fileName} />
);

const StyledDisplayFeedbacks = styled.div`
  h1 {
    font-weight: 400;
  }
  box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
  display: flex;
  .feedbacks--sidebar {
    flex: 0 0 380px;
    border-right: 1px solid #d9dcde;
  }
  .feedbacks--details-question {
    display: flex;
  }
  .feedbacks--details-question-title {
    flex: 0 0 370px;
  }
  .feedbacks--sidebar-user {
    padding: 15px;
    font-size: 16px;
    line-height: 19px;
    color: #031323;
    cursor: pointer;
    font-weight: 500;
    border-style: solid;
    border-width: 1px 0 1px 0;
    border-color: #d9dcde;
    img {
      margin-right: 15px;
    }
    &:hover {
      background: #fbf7fe;
    }
  }
  .feedback--sidebar-details {
    font-size: 12px;
    line-height: 14px;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    font-weight: 700;
    color: #59636e;
    padding: 15px;
  }
  .feedbacks--details-header {
    font-family: Untitled Sans;
    font-size: 22px;
    line-height: 26px;
    color: #031323;
    font-weight: 500;
    padding: 26px;
  }
  .feedbacks--details-question {
    padding: 26px;
    border-style: solid;
    border-width: 1px 0 1px 0;
    border-color: #d9dcde;
    &.skipped {
      color: #d9dcde;
    }
  }
  .feedbacks--details-question-value {
    padding: 0 0 0 15px;
  }
  .skipped-tag {
    background: #acb1b6;
    border-radius: 3px;
    color: white;
    font-size: 12px;
    line-height: 14px;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    width: 86px;
    padding: 5px;
    text-align: center;
    font-weight: 500;
    margin: 15px 0 0 0;
  }
`;

const getFeedbackIndex = (feedbacks, id) => {
  let found = 0;
  if (!id) id = 0;
  feedbacks.forEach((fb, index) => {
    if (fb.id === id) {
      found = index;
    }
  });
  return found;
};

const activeUser = {
  fontWeight: '700',
  background: '#F2F3F4',
};

const emptySymbol = {
  display: 'inline-block',
  background: '#f2f3f4',
  border: '2px solid #ffffff',
  boxSizing: 'border-box',
  height: '35px',
  width: '35px',
};
const fullSymbol = color => ({
  ...emptySymbol,
  background: `${color}`,
});

const generateRating = ({ value, type, options }) => {
  let max = 10;
  if (type === 'options') max = options.length;
  const percentage = Math.floor((value / max) * 100);
  let color = '#F2F3F';
  if (percentage <= 33 && percentage > 0) color = '#DE521D';
  if (percentage > 33 && percentage <= 75) color = '#F5DD07';
  if (percentage > 75) color = '#2BBF6A';
  return (
    <Rating
      initialRating={value}
      stop={max}
      readonly
      emptySymbol={emptySymbol}
      fullSymbol={fullSymbol(color)}
    />
  );
};

function DisplayFeedbacks({ feedbacks, type, id }) {
  const [currentFeedbackIndex, setCurrentFeedbackIndex] = useState(
    getFeedbackIndex(feedbacks, id),
  );
  const title = type === 'team' ? 'Team Feedback' : 'My Feedback';
  const sideBarDetails =
    type === 'team' ? 'Feedback Received' : 'Feedback Given';
  return (
    <>
      <h1 style={{ fontWeight: 500 }}>{title}</h1>
      <StyledDisplayFeedbacks>
        <div className="feedbacks--sidebar">
          <div className="feedback--sidebar-details">{sideBarDetails}</div>
          {feedbacks.map((fb, index) => (
            <div
              className="feedbacks--sidebar-user"
              style={currentFeedbackIndex === index ? activeUser : {}}
              key={fb.id}
              onClick={() => {
                setCurrentFeedbackIndex(index);
              }}
            >
              {loadImage(fb.avatar)}
              {fb.name}
            </div>
          ))}
        </div>
        <div className="feedbacks--details">
          <div className="feedbacks--details-header">
            {feedbacks[currentFeedbackIndex].name}'s Feedback
          </div>
          {feedbacks[currentFeedbackIndex].questions.map((question, id) => (
            <div
              key={id}
              className={`feedbacks--details-question ${
                question.skipped ? 'skipped' : ''
              }`}
            >
              <div className="feedbacks--details-question-title">
                {question.title}
                {question.skipped ? (
                  <div className="skipped-tag">skipped</div>
                ) : (
                  ''
                )}
                {question.type === 'text' ? <div>{question.value}</div> : ''}
              </div>
              {question.type !== 'text' ? (
                <div className="feedbacks--details-question-value">
                  {generateRating(question)}
                </div>
              ) : (
                ''
              )}
            </div>
          ))}
        </div>
      </StyledDisplayFeedbacks>
    </>
  );
}

DisplayFeedbacks.propTypes = {};

export default memo(DisplayFeedbacks);
