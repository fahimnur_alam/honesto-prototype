/**
 *
 * GeneralLayout
 *
 */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Header from '../Header';
import Footer from '../Footer';

const StyledGeneralLayout = styled.div`
  width: 100%;
  max-width: 1200px;
  min-height: calc(100% - 129px);
  margin: 0 auto;
  padding: 2rem 1rem;
`;
function GeneralLayout(props) {
  return (
    <>
      <Header />
      <StyledGeneralLayout>{props.children}</StyledGeneralLayout>
      <Footer />
    </>
  );
}

GeneralLayout.propTypes = {
  children: PropTypes.any,
};

export default memo(GeneralLayout);
