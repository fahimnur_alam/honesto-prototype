/**
 *
 * Asynchronously loads the component for GeneralLayout
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
