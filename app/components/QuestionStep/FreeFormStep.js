import React, { useState } from 'react';
import styled from 'styled-components';

const StyledText = styled.textarea`
  margin: 1rem 0;
  border: 1px solid #d9dcde;
  border-radius: 3px;
  box-sizing: border-box;
  height: 230px;
  padding: 20px;
  width: 100%;
`;

export default function FreeFormStep({ value, update }) {
  const [text, setText] = useState(value);
  const updateText = v => {
    setText(v);
    update(v);
  };
  return (
    <StyledText
      placeholder="Say something"
      onChange={e => updateText(e.target.value)}
      value={text}
    />
  );
}
