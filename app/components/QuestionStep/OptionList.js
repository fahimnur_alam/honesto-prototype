import React, { useState } from 'react';
import styled from 'styled-components';

const isSelected = {
  background: '#59636E',
  color: '#fff',
};
const StyledOption = styled.div`
  background: #f2f3f4;
  border-radius: 3px;
  padding: 20px;
  margin: 10px 0;
  cursor: pointer;
`;
export default function OptionList({ value, options, update }) {
  const [selected, setSelected] = useState(value);
  const selectOption = key => {
    setSelected(key);
    update(key + 1);
  };
  return options.map((option, key) => (
    <StyledOption
      key={key}
      style={selected === key ? isSelected : {}}
      onClick={() => selectOption(key)}
    >
      {option}
    </StyledOption>
  ));
}
