import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Rating from 'react-rating';
import OptionList from './OptionList';
import RatingStep from './RatingStep';
import FreeFormStep from './FreeFormStep';

const loadImage = fileName => (
  <img src={require(`assets/avatars/${fileName}.svg`)} alt={fileName} />
);

const StyledStep = styled.div`
  .question-step--wrapper {
    box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
    padding: 10px 20px;
  }
  p.share-feedback {
    font-size: 12px;
    line-height: 14px;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    color: #acb1b6;
    font-weight: 500;
  }
  h1 {
    font-size: 31px;
    line-height: 36px;
    color: #031323;
    font-weight: 500;
  }
  .question-step--header {
    display: flex;
    justify-content: space-between;
    align-content: flex-start;
  }

  progress::-moz-progress-bar,
  progress::-webkit-progress-bar {
    background-color: #f3f4f5;
    width: 100%;
    border-radius: 15px;
  }

  progress::-webkit-progress-value {
    background: #4aead2;
    border-radius: 15px;
  }

  progress {
    background: #4aead2;
    color: #4aead2;
    width: 100%;
    height: 5px;
    border-radius: 15px;
  }

  .completed-questions {
    font-size: 12px;
    line-height: 14px;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    font-weight: 700;
  }

  .questions-footer {
    display: flex;
    justify-content: space-between;
    align-content: flex-start;
  }
  .questions-footer--status {
    font-weight: 500;
  }

  .star {
    position: relative;
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 0.9em;
    margin-right: 0.9em;
    margin-bottom: 1.2em;
    border-right: 0.3em solid transparent;
    border-bottom: 0.7em solid #fc0;
    border-left: 0.3em solid transparent;
    font-size: 10px;
    &:before,
    &:after {
      content: '';
      display: block;
      width: 0;
      height: 0;
      position: absolute;
      top: 0.6em;
      left: -1em;
      border-right: 1em solid transparent;
      border-bottom: 0.7em solid #fc0;
      border-left: 1em solid transparent;
      transform: rotate(-35deg);
    }
    &:after {
      transform: rotate(35deg);
    }
  }
  .empty-star {
    border-bottom: 0.7em solid #acb1b6;
    &:before,
    &:after {
      border-bottom: 0.7em solid #acb1b6;
    }
  }
  .question-steps-navigation {
    display: flex;
    justify-content: space-between;
    align-content: flex-start;
    margin: 30px 0;
  }
  .questions-steps-metadata {
    margin: 30px 0;
  }
`;

const emptySymbol = {
  display: 'inline-block',
  background: '#f2f3f4',
  border: '2px solid #ffffff',
  boxSizing: 'border-box',
  height: '74px',
  width: '74px',
};
const fullSymbol = {
  ...emptySymbol,
  background: '#AB61E5',
};

const hide = {
  display: 'none',
};

export default function Step({
  question,
  qid,
  feedback,
  nextStep,
  previousStep,
  currentStep,
  totalSteps,
  onComplete,
}) {
  const skip = () => {
    feedback.questions[qid].skipped = true;
    nextStep();
  };

  const updateFeedback = v => {
    feedback.questions[qid].value = v;
  };

  const questionType = ({ value, type, options, description }) => {
    switch (type) {
      case 'text':
        return <FreeFormStep value={value} update={updateFeedback} />;
      case 'options':
        return (
          <OptionList options={options} value={value} update={updateFeedback} />
        );
      case 'rating':
        return (
          <RatingStep
            description={description}
            value={value}
            update={updateFeedback}
          />
        );
      default:
        return null;
    }
  };

  const passData = () => {
    onComplete(feedback);
  };

  const currentProgress = currentStep / totalSteps;

  return (
    <StyledStep>
      <div className="question-step--header">
        <div>
          <h1>{question.title}</h1>
          <p className="share-feedback">{`Share your feedback for ${
            feedback.name
          }`}</p>
        </div>
        {loadImage(feedback.avatar)}
      </div>
      <div className="question-step--wrapper">
        {questionType(question)}

        <div className="question-steps-navigation">
          <button
            style={currentStep === 1 ? hide : {}}
            onClick={previousStep}
            className="secondary"
          >
            Previous
          </button>
          <button
            style={currentStep === totalSteps ? hide : {}}
            onClick={skip}
            className="secondary"
          >
            Skip
          </button>
          <button
            style={currentStep === totalSteps ? hide : {}}
            onClick={nextStep}
            className="secondary"
          >
            Next
          </button>
          <button
            style={currentStep === totalSteps ? {} : hide}
            onClick={passData}
            className="secondary"
          >
            Submit
          </button>
        </div>

        <progress max="1" value={currentProgress} />

        <div className="questions-steps-metadata">
          <div className="completed-questions">Questions Completed</div>
          <div className="questions-footer">
            <div className="questions-footer--status">
              {currentStep}/{totalSteps}
            </div>
            <div className="questions-footer--rating">
              <Rating
                initialRating={question.rating}
                readonly
                stop={3}
                fullSymbol="star"
                emptySymbol="star empty-star"
              />
            </div>
          </div>
        </div>
      </div>
    </StyledStep>
  );
}
Step.propTypes = {
  question: PropTypes.object,
  qid: PropTypes.number,
  feedback: PropTypes.object,
  nextStep: PropTypes.func,
  previousStep: PropTypes.func,
  onComplete: PropTypes.func,
  currentStep: PropTypes.number,
  totalSteps: PropTypes.number,
};
