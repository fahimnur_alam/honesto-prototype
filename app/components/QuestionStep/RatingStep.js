import React, { useState } from 'react';
import styled from 'styled-components';
import Rating from 'react-rating';

const StyledRating = styled.div`
  p {
    margin: 1rem 0 2rem 0;
    color: #031323;
  }
  .current-rating {
    text-align: right;
    color: #031323;
    margin: 7px 0;
  }
`;

const emptySymbol = {
  display: 'inline-block',
  background: '#f2f3f4',
  border: '2px solid #ffffff',
  boxSizing: 'border-box',
  height: '74px',
  width: '74px',
};
const fullSymbol = {
  ...emptySymbol,
  background: '#AB61E5',
};

export default function RatingStep({ description, value, update }) {
  const [rating, setRating] = useState(value);
  const updateRating = v => {
    setRating(v);
    update(v);
  };
  return (
    <StyledRating>
      <p>{description}</p>
      <Rating
        onClick={updateRating}
        initialRating={rating}
        stop={10}
        emptySymbol={emptySymbol}
        fullSymbol={fullSymbol}
      />
      <div className="current-rating">{rating}/10</div>
    </StyledRating>
  );
}
