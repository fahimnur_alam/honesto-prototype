/**
 *
 * EmptyFeedback
 *
 */

import React, { memo } from 'react';
import styled from 'styled-components';

const StyledBox = styled.div`
  padding: 5rem;
  background: #ffffff;
  box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
`;

function EmptyFeedback() {
  return (
    <StyledBox>
      <h1>No feedback to display 🔮</h1>
      No feedback to display There is no feedback to display at this time -
      check in a bit!
    </StyledBox>
  );
}

EmptyFeedback.propTypes = {};

export default memo(EmptyFeedback);
