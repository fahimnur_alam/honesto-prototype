/**
 *
 * Footer
 *
 */

import React from 'react';
import logo from 'assets/images/theorem-logo.svg';
import styled from 'styled-components';

const StyledFooter = styled.footer`
  background-color: #031323;
  font-size: 12px;
  color: #fff;
  bottom: 0;
  width: 100%;
  .wrapper {
    align-items: center;
    display: flex;
    height: 54px;
    max-width: 1200px;
    margin: auto;
  }
  .right {
    margin-left: auto;
    text-align: right;
    padding: 0 1rem;
  }
  .left {
    margin-right: auto;
    text-align: left;
    padding: 0 1rem;
  }
`;

function Footer() {
  return (
    <StyledFooter>
      <div className="wrapper">
        <div className="left">
          <img src={logo} alt="theorem" />
        </div>
        <div className="right">
          Copyright © 2018 <strong>Theorem</strong>, LLC. All Rights Reserved.
        </div>
      </div>
    </StyledFooter>
  );
}

Footer.propTypes = {};

export default Footer;
