/**
 *
 * FeedbackStep
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function FeedbackStep({
  currentStep,
  totalSteps,
  previousStep,
  nextStep,
  goToStep,
  firstStep,
  lastStep,
  isActive,
}) {
  return (
    <div>
      <h2>Step {currentStep}</h2>
      <p>Total Steps: {totalSteps}</p>
      <p>Is Active: {isActive}</p>
      <p>
        <button onClick={previousStep}>Previous Step</button>
      </p>
      <p>
        <button onClick={nextStep}>Next Step</button>
      </p>
      <p>
        <button onClick={() => goToStep(2)}>Step 2</button>
      </p>
      <p>
        <button onClick={firstStep}>First Step</button>
      </p>
      <p>
        <button onClick={lastStep}>Last Step</button>
      </p>
    </div>
  );
}

FeedbackStep.propTypes = {};

export default memo(FeedbackStep);
