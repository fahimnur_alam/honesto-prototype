/**
 *
 * ShareList
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const StyledShare = styled.div`
  background: #ffffff;
  box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
  margin-top: 1rem;
`;
const StyledFeedback = styled.div`
  border-bottom: 1px solid rgba(172, 177, 182, 0.33);
  height: 90px;
  padding: 15px 25px;
  display: flex;
  justify-content: space-between;

  }
  button {
    width: 180px;
    font-size: 16px;
    padding: 15px 10px;
    font-weight: 500;
  }
  .feedback--name {
    font-size: 22px;
    line-height: 26px;
    margin-left: 30px;
    color: #59636E;
  }
`;

const loadImage = fileName => (
  <img src={require(`assets/avatars/${fileName}.svg`)} alt={fileName} />
);

const Feedback = ({ feedback }) => (
  <StyledFeedback>
    <div>
      {loadImage(feedback.avatar)}
      <span className="feedback--name">{feedback.name}</span>
    </div>
    {feedback.completed ? (
      <Link to={`/my-feedback/${feedback.id}`}>
        <button className="secondary">View Submission</button>
      </Link>
    ) : (
      <Link to={`/share-feedback/${feedback.id}`}>
        <button>Fill Out</button>
      </Link>
    )}
  </StyledFeedback>
);

function ShareList({ feedbacks }) {
  return (
    <StyledShare>
      {feedbacks.map(feedback => (
        <Feedback feedback={feedback} key={feedback.id} />
      ))}
    </StyledShare>
  );
}

Feedback.propTypes = {
  feedback: PropTypes.object,
};

ShareList.propTypes = {
  feedbacks: PropTypes.array,
};

export default memo(ShareList);
