/**
 *
 * Asynchronously loads the component for ShareList
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
