import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import store from 'store';

export function isLoggedIn() {
  return store.get('token');
}

export default function PrivateRoute({
  component: Component,
  authed,
  ...rest
}) {
  return (
    <Route
      {...rest}
      render={props =>
        isLoggedIn() ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: '/login', state: { from: props.location } }}
          />
        )
      }
    />
  );
}
