## Links

App hosted w/ Continuous Deployment via Netlify: https://amazing-galileo-dc16f0.netlify.com/

Project management: [Trello Board](https://trello.com/b/yvpiFmZM/theorem-honesto)

## Quick start

1. Make sure that you have Node.js v8.15.1 and npm v5 or above installed.
2. Clone this repo using `git clone git@bitbucket.org:fahimnur_alam/honesto-prototype.git`
3. Move to the appropriate directory: `cd honesto-prototype`.
4. Run `npm run setup` in order to install dependencies.

_At this point you can run `npm start` to see the example app at `http://localhost:3000`._

5. Run `npm run build` to build the application for production

## De-scoped Items

1. Google Sign In
2. Back-end API
3. Counters in Nav Bar.
4. New Feedback
5. Feedback Period Filter (sort by created - already available in seed data)
6. Flagging a question
7. Teams --- no designs available to work off.
8. Responsive Design
9. Tooltips in Feedback
10. Skeleton Loading

## If I had 1 Week

1. Better CSS architecture
2. More code refractoring to make the application elegant and easier for other developers.
3. Polish up the application and implement tests (e2e, unit).
4. More comments in code

I was able to complete all the scoped items from the initial call. I didn't want to disappoint the client, so I too my time to plan everything out.

## De-de-scoped items

These items were originally de-scoped, but since I was in the "zone" and was getting things done quick, I thought I could just implement them. Of course, after I was done with the high priority tasks.

1. Question skipping
2. Progress bar in wizard
3. Display to user how many questions they have completed
4. Display question rating
5. Feedback cycle (design)
